# TramREbot

Bot di Telegram nato per controllare quanto manca all'arrivo del tram nella zona di Reggio Emilia.

Ora puo' essere usato anche per le zone di Modena e Piacenza.

Il bot risponde su Telegram agli username:
- [@tramREbot](https://t.me/tramREbot): Reggio Emilia
- [@busMObot](https://t.me/busMObot): Modena
- [@autobusPCbot](https://t.me/autobusPCbot): Piacenza

Per richieste/consigli utilizzare la sezione [Issues](https://gitlab.com/michirod/tramrebot/issues)
