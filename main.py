#!/usr/bin/env python3
# tramREbot
# Copyright (C) 2018-2020
# Michirod <michirod@ciocapiat.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import configparser
import tram_bot

def parse_config(filename: str) -> list:
    """parse the ini config file and create the configures bots"""
    bots = []
    config = configparser.ConfigParser()
    config.read(filename)
    for bot_config in config.sections():
        if config[bot_config]['status'] == 'okay':
            bot = tram_bot.TramBot(config[bot_config])
            print("Created {}".format(bot))
            bots.append(bot)
    return bots

def main():
    """Main."""
    if len(sys.argv) < 2:
        print("Error: please provide configuration file")
        sys.exit(-1)
    bots = parse_config(sys.argv[1])
    for bot in bots:
        bot.start()

main()
