# tramREbot
# Copyright (C) 2018-2020
# Michirod <michirod@ciocapiat.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import urllib.request
import urllib.parse
import json
import re
import ssl
from datetime import datetime
from abc import ABC, abstractmethod
import utm
from bs4 import BeautifulSoup

class StopStatus:
    def __init__(self, name, stop_id):
        self.stop_name = name
        self.stop_id = stop_id
        self.datetime = None
        self.lines = []
        self.errors = []
        self.other = ""


class TransportService(ABC):
    """Abstract class for transport service interface"""

    stops_by_name = {}
    stops_by_id = {}

    @abstractmethod
    def get_all_stops(self) -> list:
        """Return the list of all the bus stops"""

    @abstractmethod
    def filter_stops(self, text: str) -> list:
        """Return stops filtered by text"""

    @abstractmethod
    def find_near_stops(self, location) -> list:
        """Return stops close to location"""

    @abstractmethod
    def find_radius_stops(self, location, radius: int) -> list:
        """Return stops in a radius from location"""

    @abstractmethod
    def get_current_status(self, stop_id: str) -> object:
        """Return current waiting times for a given stop_id"""

def get_transport_service(name: str, *args, **kwargs):
    """Factory method for TransportService instances

    Args:
        name (:obj:`str`): name of the transport service.
        args (:obj:`list`, optional): optional arguments to be passed to
            transport service constructor.
        kwargs (:obj:`dict`, optional): optional arguments to be passed to
            transport service constructor.

    """

    if name.lower() == "seta":
        if kwargs is not None and "seta_bacino" in kwargs:
            return SetaService(bacino=kwargs["seta_bacino"])
        if args is not None and isinstance(args[0], dict):
            return SetaService(bacino=args[0]["seta_bacino"])
    raise RuntimeError(name)

class SetaService(TransportService):
    """Manages a SETA transport service

    Args:
        bacino (:obj:`str`): SETA transport service territory.

    """

    def __init__(self, bacino: str):
        self.bacino = bacino
        self.ssl_ctx = ssl._create_unverified_context()
        self.url = "https://www.setaweb.it/" + bacino + "/quantomanca"
        self.url_funzioni = "https://www.setaweb.it/funzioni/funzioni.php"
        self.stops_json = self.get_all_stops()
        stop_ids_from_form = self.get_stop_ids_from_form()
        self.stops_by_name = {stop["name"]: stop for stop in self.stops_json
                              if stop["stop_id"] in stop_ids_from_form}
        self.stops_by_id = {stop["stop_id"]: stop for stop in self.stops_json}

    def __str__(self):
        return "Seta, bacino: " + self.bacino

    def get_stop_ids_from_form(self):
        """Get the stop ids from the web form
        (used for quantomanca stop search)"""

        try:
            soup = BeautifulSoup(urllib.request.urlopen(self.url,
                                                        context=self.ssl_ctx),
                                 "lxml")
            options = soup.select("form#seleziona_palina select option")
            ids = [opt["value"] for opt in options
                   if "value" in opt.attrs.keys()]
            return ids
        except:
            print("Error")
            raise


    def get_all_stops(self) -> list:
        """Get all the stops.

        This function retrieves the stop data using the 'funzioni' page
        which returns all the stops. Some stops could have the same name
        but different stop_id"""

        # search in the whole area
        result = self.find_radius_stops(None, -1)
        #print(result)
        return result

    def filter_stops(self, text: str) -> list:
        """Get all the stops that contain the text.

        This function uses the stops retrieved from the website search form to
        to simulate a form search. Stop names are unique."""

        res = [val for key, val in self.stops_by_name.items()
               if text.upper() in key.upper()]
        return res

    def find_near_stops(self, location) -> list:
        """Find nearby stops.

        This function retrieves the stop data using the 'funzioni' page
        which returns all the stops. Some stops could have the same name
        but different stop_id"""

        radius = 100
        stops = self.find_radius_stops(location, radius)
        while len(stops) < 3:
            radius = radius * 2
            stops = self.find_radius_stops(location, radius)
        return stops

    def find_radius_stops(self, location, radius: int) -> list:
        """Find stops within a radius. If radius < 0 search the whole area

        This function retrieves the stop data using the 'funzioni' page
        which returns all the stops. Some stops could have the same name
        but different stop_id"""

        if radius < 0:
            # search in the whole area
            data = urllib.parse.urlencode([
                ("funzione", "get_paline_area_selezionata"),
                ("bacino", self.bacino),
                ("NELat", (-1) % 1000000),
                ("NELon", (-1) % 1000000),
                ("SWLat", (0)  % 1000000),
                ("SWLon", (0)  % 1000000)
                ])
        else:
            utm_coord = utm.from_latlon(location.latitude, location.longitude)
            # search in a radius (meters)
            data = urllib.parse.urlencode([
                ("funzione", "get_paline_area_selezionata"),
                ("bacino", self.bacino),
                ("NELat", (utm_coord[1] + radius) % 1000000),
                ("NELon", (utm_coord[0] + radius)),
                ("SWLat", (utm_coord[1] - radius) % 1000000),
                ("SWLon", (utm_coord[0] - radius)),
                ])

        try:
            response = urllib.request.urlopen(self.url_funzioni, data.encode(),
                                              context=self.ssl_ctx)
            seta_stops = json.loads(response.read().decode())
            result = [{"stop_id": stop["Palina"],
                       "line": stop["Linea"],
                       "name": stop["Nome"],
                       "long": stop["Longitudine"],
                       "lat": stop["Latitudine"]}
                      for stop in seta_stops]
        except Exception as ex:
            print(ex)
            result = "Il sito setaweb non sta funzionando come dovuto"
            raise
        return result

    def get_current_status(self, stop_id: str) -> object:
        """Get updated data from the website."""
        name = self.stops_by_id[stop_id]["name"]
        status = StopStatus(name, stop_id)
        data = urllib.parse.urlencode([
            ("risultato", "palina"),
            ("nome_fermata", name),
            ("qm_palina", stop_id),
            ])
        try:
            soup = BeautifulSoup(urllib.request.urlopen(self.url, data.encode(),
                                                        context=self.ssl_ctx),
                                 "lxml")
            content = soup.find(class_="contenutobacino")
            time = content.findAll(class_="qm_titolo blu")[1].text
            time = re.sub('.*ore', 'ore', time)
            status.datetime = datetime.strptime(time, "ore %H:%M del %d/%m/%Y")
            # remove useless stuff (there is a
            # misleading table.qm_table_risultati in the bottom section)
            content.find(class_="qm_blocco").decompose()
            #print(content)
            table_risultati = content.select_one("table.qm_table_risultati")
            if table_risultati is None:
                #this is the case when the website says "data not available"
                info = [repr(s) for s in content.select_one(
                    ".qm_blocco .centrata").stripped_strings]
                status.other += 'SETA dice:\n' + '\n'.join(info)
                table = []
            else:
                table = table_risultati.find("tbody").findAll("tr")
        except Exception as ex:
            print("error")
            print(ex)
            status.errors.append(
                "Il sito setaweb.it non sta funzionando come dovuto")
            status.errors.append(ex)
            return status
        for element in table:
            row = [k.text.replace("\n", "") for k in element.findAll("td")]
            print(row)
            if len(row) < 4:
                # this is the case when the website says
                # "no scheduled buses in 60 min"
                status.other += row[0] + "\n"
            elif '*' in row[2]:
                # no realtime information available
                status.lines.append({'line': row[0],
                                     'dest': row[1],
                                     'realtime_due': None,
                                     'scheduled_time': row[3],
                                     'realtime_time': None})
            else:
                #realtime
                status.lines.append({'line': row[0],
                                     'dest': row[1],
                                     'realtime_due': row[2],
                                     'scheduled_time': row[3],
                                     'realtime_time': row[4]})
        return status
