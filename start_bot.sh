#!/bin/bash
PROJECT_NAME="Tram Bot"

CONFIG_DIR="$(dirname "${BASH_SOURCE[0]}")"
CONFIG_FILE="$CONFIG_DIR/.config.ini"
TEMPLATE_FILE="$(dirname "${BASH_SOURCE[0]}")/template_config.ini"

if test ! -d "$CONFIG_DIR" -o ! -x "$CONFIG_FILE"; then
	if test ! -d "$CONFIG_DIR"; then
		mkdir -p "$CONFIG_DIR"
	fi
	if test ! -f "$CONFIG_FILE"; then
		cp "$TEMPLATE_FILE" "$CONFIG_FILE"
	fi
fi

if test -z "$EDITOR"; then
	EDITOR='vim'
fi

if test "$1" = "-e"; then
	$EDITOR $CONFIG_FILE
elif test -n "$1"; then
	echo "$(basename "${BASH_SOURCE[0]}") - start $PROJECT_NAME"

	echo
	echo "Usage:"
	echo -e "\t$(basename "${BASH_SOURCE[0]}") [OPTIONS]"
	echo
	echo "OPTIONS:"
	echo -e "\t-e : edit config file before launch"
	echo -e "\t-h : show this help"
	echo
	if test "$1" != "-h" -a "$1" != "--help"; then
		echo "unknown option \"$1\""
		exit 1
	fi
	exit 0
fi

echo Starting $PROJECT_NAME
if [[ ! -d "$CONFIG_DIR/venv" ]]; then
	python3 -m venv "$CONFIG_DIR/venv"
fi
source $CONFIG_DIR/venv/bin/activate
python3 "$(dirname "${BASH_SOURCE[0]}")"/main.py $CONFIG_FILE
