# tramREbot
# Copyright (C) 2018-2020
# Michirod <michirod@ciocapiat.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
from datetime import datetime
from telegram import Update
from telegram import InlineQueryResultArticle, InputTextMessageContent
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackContext, ApplicationBuilder
from telegram.ext import InlineQueryHandler, CallbackQueryHandler
import transport_service

class MessageData:
    """Data used by callbacks. It contains stop name and id and some flags"""

    def __init__(self, name: str, stop_id: str):
        self.name = name
        self.stop_id = stop_id
        self.flags = {"info": False}

    def flag(self, flag: str) -> bool:
        """Returns flag value"""
        return self.flags[flag]

    def set_flags(self, flags: dict) -> None:
        """Set flags as new flags"""
        self.flags = flags

    def toggle_flag(self, flag: str) -> None:
        """Change the value of the flag"""
        self.flags[flag] = not self.flags[flag]

    def encode(self, toggle_flags=None, set_flags=None, unset_flags=None) -> str:
        """Serialize the object as a json string"""
        tmp_flags = self.flags.copy()
        if toggle_flags is not None:
            for flag in toggle_flags:
                tmp_flags[flag] = not tmp_flags[flag]
        if set_flags is not None:
            for flag in set_flags:
                tmp_flags[flag] = True
        if unset_flags is not None:
            for flag in unset_flags:
                tmp_flags[flag] = False
        return json.dumps([self.stop_id, self.name, tmp_flags],
                          separators=(',', ':'))

    @staticmethod
    def decode(data_string: str) -> object:
        """Deserialize the object from a json string"""
        try:
            data = json.loads(data_string)
        except json.decoder.JSONDecodeError:
            # backward compatibility
            data = data_string.split(';')
            msg_data = MessageData(data[1], data[0])
            return msg_data
        msg_data = MessageData(data[1], data[0])
        msg_data.set_flags(data[2])
        return msg_data


def prepare_response(stop_status: object) -> str:
    """Get updated data from the website."""

    text = "Fermata di *" + stop_status.stop_name + "*\n"
    text += "_" + stop_status.stop_id + "_\n"
    text += datetime.strftime(stop_status.datetime,
                              "ore %H:%M del %d/%m/%Y\n\n")
    #test = ""
    #for error in stop_status.errors:
    #    test += error + '\n'
    for line in stop_status.lines:
        if line['dest'] == '_':
            line['dest'] = 'Direzione non disponibile'
        if line['realtime_due'] is None:
            text += '\U000023f1 *{}* \t {} alle {}\'\n'.format(
                line['line'],
                line['dest'],
                line['scheduled_time'])
        else:
            text += '\U0001f6f0 *{}* \t {} tra {}\'\n'.format(
                line['line'],
                line['dest'],
                line['realtime_due'])
    if stop_status.other != "":
        text += stop_status.other + '\n'
    return text

def show_info() -> str:
    """Generate a info string"""
    return ("\n*Legenda*\n"
            "\U000023f1 \t da orario\n"
            "\U0001f6f0 \t tempo reale")

async def button_callback(update: Update, context: CallbackContext) -> None:
    """Update the message content."""
    query = update.callback_query
    print('{}: {} - query from {} {} {} {}: {}'
          .format(datetime.now(), context.bot.username, query.from_user.id,
                  query.from_user.first_name, query.from_user.last_name,
                  query.from_user.username, query.data), flush=True)
    data = MessageData.decode(query.data)
    bot = context.bot_data['bot']
    stop_status = bot.service.get_current_status(data.stop_id)
    text = prepare_response(stop_status)
    if data.flag('info'):
        text += show_info()
    info_status = '\u2753' if data.flag('info') else '\u2754'
    keyboard = [[InlineKeyboardButton("Aggiorna",
                                      callback_data=data.encode()),
                 InlineKeyboardButton("Info " + info_status,
                                      callback_data=data.encode(
                                          toggle_flags=['info']))]]
    try:
        await query.edit_message_text(text=text,
                                      parse_mode="Markdown",
                                      reply_markup=InlineKeyboardMarkup(keyboard))
    except:
        await query.edit_message_text(text="Come prima...\n"+text,
                                      parse_mode="Markdown",
                                      reply_markup=InlineKeyboardMarkup(keyboard))

    await query.answer()

async def help_command(update: Update, context: CallbackContext) -> None:
    """Send help message."""
    await update.message.reply_text(
              context.bot_data["bot"].help_message
              .replace("\n", " ")
              .replace("\\n", "\n")
              .format(update.message.from_user.first_name, context.bot.username))

def prepare_inline_query_results(items: list) -> list:
    """Prepare inline query results."""
    results = list()
    for info in items:
        keyboard = [[InlineKeyboardButton(
            "Aggiorna",
            callback_data=MessageData(info["name"][:20], # limit name size
                                      info["stop_id"])
            .encode())]]
        results.append(
            InlineQueryResultArticle(
                id=info["stop_id"],
                title=info["name"],
                description=info["stop_id"],
                input_message_content=InputTextMessageContent(
                    "Fermata di *" + info["name"] +
                    "*\n_" + info["stop_id"] + "_\n"
                    "\nSchiaccia il bottone per aggiornare",
                    parse_mode="Markdown"),
                reply_markup=InlineKeyboardMarkup(keyboard)
            )
        )
    return results

async def inline_callback(update: Update, context: CallbackContext) -> list:
    """Handle inline callback."""
    query = update.inline_query.query
    location = update.inline_query.location
    bot = context.bot_data['bot']
    if location and len(query) == 0:
        matching_stops = bot.service.find_near_stops(location)
        personal = True
    else:
        matching_stops = bot.service.filter_stops(query)
        personal = False
    #print(matching_stops)
    results = prepare_inline_query_results(matching_stops)
    #print('matching stops for {}: {}'.format(query, len(matching_stops)))
    await context.bot.answer_inline_query(update.inline_query.id, results,
                                          next_offset='',
                                          is_personal=personal)


class TramBot:
    def __init__(self, config):
        self.name = config.name
        self.telegram_token = config["telegram_token"]
        self.help_message = config["help_message"]
        # pass all the config values to the transport service constructor
        # since the required ones are not known.
        self.service = transport_service.get_transport_service(
            config["transport_service"],
            {tup[0]: tup[1] for tup in config.items()})
        # initialize the bot
        self.application = ApplicationBuilder().token(self.telegram_token).build()
        self.application.bot_data["bot"] = self

    def __str__(self):
        return "Bot {}: transport {}".format(self.name, self.service)

    def start(self):
        self.application.add_handler(
            CommandHandler('start', help_command))
        self.application.add_handler(
            CommandHandler('help', help_command))
        self.application.add_handler(
            InlineQueryHandler(inline_callback))
        self.application.add_handler(
            CallbackQueryHandler(button_callback))
        self.application.run_polling()
